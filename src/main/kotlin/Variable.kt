import io.reactivex.Observable
import io.reactivex.Observer

class Variable<T>(item: T) : Observable<T>() {

    private var observers = arrayListOf<Observer<in T>>()
    var item = item
    set(value) {
        field = value
        observers.forEach {
            observer -> observer.onNext(value)
        }
    }

    override fun subscribeActual(observer: Observer<in T>?) {
        observer?.let {
            observers.add(observer)
            observer.onNext(item)
        }
        

    }
}